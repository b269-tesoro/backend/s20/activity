// console.log("Hello World");
function numberLoop()
{
	let num = prompt("Provide a number: ");
	console.log("The number you provided is "+ num);

	for(let i = num; i>0; i--)
	{
		if(i<=50) {
			console.log("The current value is at "+ i + ". Terminating the loop.")
			break;
		}

		if(i%10 === 0)
		{
			console.log("The number is divisible by 10. Skipping the number.")
			continue;
		}

		if(i%5 === 0){
			console.log(i);
		}
	}
}

numberLoop();

function removeVowels(string)
{
	// creating a string variable to store the word without vowels
	let result = ""
	for(let i=0; i<string.length; i++) 
	{
		if (string[i] === "a" ||
			string[i] === "e" ||
			string[i] === "i" ||
			string[i] === "o" ||
			string[i] === "u" ) {
			continue;
		}
		
		else {
		result+= string[i];
		}
	}
		
	console.log(result);
}

let word = "supercalifragilisticexpialidocious";
removeVowels(word);